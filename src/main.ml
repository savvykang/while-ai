open Interpreter

let filename = ref ""
let anony fname = if Sys.file_exists fname then filename := fname else ()

let debug = true

let on_print = ref false
let on_trace = ref false
let on_domain = ref false
let on_test = ref false

let analysis_str = ref ""

let usage = "
Mini Project: WHILE language by HyunGoo Kang

Usage: < executablefile > < options >
Options are: "

let arglist = (* 옵션     동작                설명 *)
	[ "-t", (Arg.Set on_trace), "Print the program trace to standard output";
	]

let arglist =
	if debug
	then arglist @ [("--test", (Arg.Set on_test), "test point")]
	else arglist

let run () =
	Arg.parse arglist anony usage;
	let input_channel = stdin in
	let lexbuf = Lexing.from_channel input_channel in
	let ast = Analyzer.number (Parser.main Lexer.lexer lexbuf) in
	if !on_test
	then
	Interpreter.testcase ()
	else
	ignore(Interpreter.eval_stmt !on_trace BatPMap.empty ast)

let _ = Printexc.catch run ()