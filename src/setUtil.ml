
let singleton e = BatPSet.add e BatPSet.empty

let intersection s1 s2 =
	let (s1, s2) =
		if (BatPSet.cardinal s1) < (BatPSet.cardinal s2)
		then (s1, s2)
		else (s2, s1)
	in BatPSet.filter (fun x -> (BatPSet.mem x s1) && (BatPSet.mem x s2)) s1

let product a b =
	let op = fun x y -> (x, y) in
	let foo = BatList.of_enum (BatPSet.enum b) in
	let bar = BatList.of_enum (BatPSet.enum a) in
	let baz = (fun x -> BatList.map (fun y -> op x y) foo )in
	let foobar =
		try BatList.reduce BatList.append (BatList.map baz bar)
		with _ -> [] in
	BatPSet.of_enum (BatList.enum foobar)

let subsetof a b = BatPSet.for_all (fun x -> BatPSet.mem x b) a