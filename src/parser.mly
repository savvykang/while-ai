%{
  open Ast
%}

%token <int> INT
%token <bool> BOOL
%token <Ast.id> VAR
%token PLUS MINUS TIMES DIVIDE MODULO
%token AND OR
//%token NOT
%token LESSTHAN GREATERTHAN LTEQUAL GTEQUAL EQUAL NOTEQUAL 
%token LPAREN RPAREN
%token LBRACE RBRACE 
%token ASSIGN SKIP SEMICOLON
%token IF ELSE
%token WHILE
%token PRINT
%token NEWLINE
%token EOF

%left AND OR
%left PLUS MINUS
%left TIMES DIV
%right SEMICOLON
%right ELSE
%nonassoc NOT

%start main
%type <Ast.stmt> main

%%

main:
  | stmt EOF                { $1 }
;

stmt:
  | VAR ASSIGN aexpr        { StmtWithBlock(Assign($1, $3, 0)) }
  | SKIP                    { StmtWithBlock(Skip(0)) }
  | stmt SEMICOLON stmt     { Seq($1, $3) }
  | IF LPAREN bexpr RPAREN LBRACE stmt RBRACE ELSE LBRACE stmt RBRACE
                            { StmtWithBlock(Cond($3, $6, $10, 0)) }
  | WHILE LPAREN bexpr RPAREN LBRACE stmt RBRACE
                            { StmtWithBlock(While($3, $6, 0)) }
  | PRINT aexpr             { Print($2) }
;

aexpr:
  | INT                       { Int($1) }
  | VAR                       { Var($1) }
  | aexpr PLUS aexpr          { Arith(Plus, $1, $3) }
  | aexpr MINUS aexpr         { Arith(Minus, $1, $3) }
  | aexpr TIMES aexpr         { Arith(Times, $1, $3) }
  | aexpr DIVIDE aexpr        { Arith(Divide, $1, $3) }
	| aexpr MODULO aexpr        { Arith(Modulo, $1, $3) }
  | LPAREN aexpr RPAREN       { $2 }
;

bexpr: 
  | BOOL                      { Bool($1) }
  | bexpr AND bexpr           { BoolOp(And, $1, $3) }
  | bexpr OR bexpr            { BoolOp(Or, $1, $3) }
  | aexpr LESSTHAN aexpr      { Relation(LessThan, $1, $3) }
  | aexpr GREATERTHAN aexpr   { Relation(GreaterThan, $1, $3) }
  | aexpr LTEQUAL aexpr       { Relation(LTEqual, $1, $3) }
  | aexpr GTEQUAL aexpr       { Relation(GTEqual, $1, $3) }
  | aexpr EQUAL aexpr         { Relation(Equal, $1, $3) }
  | aexpr NOTEQUAL aexpr      { Relation(NotEqual, $1, $3) }        
  | LPAREN bexpr RPAREN       { $2 }
;