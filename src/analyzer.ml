open Ast
open SetUtil

exception WTF of string

let labelof block = match block with
	| Assign (_, _, l) -> l
	| Skip l -> l
	| Cond (_, _, _, l) -> l
	| While (_, _, l) -> l

let rec init stmt = match stmt with
	| StmtWithBlock b -> labelof b
	| Seq (s1, s2) -> (try init s1 with | NoProgramPoint -> init s2)
	| _ -> raise NoProgramPoint

let rec final stmt = match stmt with
	| StmtWithBlock (Cond (_, s1, s2, l)) ->
		BatPSet.union (try final s1 with | NoProgramPoint -> singleton l)
			(try final s2 with | NoProgramPoint -> singleton l)
	| StmtWithBlock b -> singleton (labelof b)
	| Seq (s1, s2) -> (try final s2 with | NoProgramPoint -> final s1)
	| _ -> raise NoProgramPoint

let rec blocks stmt = match stmt with
	| StmtWithBlock block ->
		BatPSet.add block
			(match block with
			| Cond (_, s1, s2, _) -> BatPSet.union (blocks s1) (blocks s2)
			| While (_, s, _) -> blocks s
			| _ -> BatPSet.empty)
	| Seq (s1, s2) -> BatPSet.union (blocks s1) (blocks s2)
	| _ -> BatPSet.empty

let rec labels stmt = match stmt with
	| StmtWithBlock block ->
		BatPSet.add (labelof block) (match block with
			| Cond (_, s1, s2, _) -> BatPSet.union (labels s1) (labels s2)
			| While (_, s, _) -> labels s
			| _ -> BatPSet.empty)
	| Seq (s1, s2) -> BatPSet.union (labels s1) (labels s2)
	| _ -> BatPSet.empty

let labels_r labels =
	BatPMap.of_enum (BatEnum.map (fun (x, y) -> (y, x)) (BatPMap.enum labels))

let rec flow stmt = match stmt with
	| StmtWithBlock (Assign _) | StmtWithBlock (Skip _) -> BatPSet.empty
	| Seq (s1, s2) ->
		List.fold_left BatPSet.union BatPSet.empty
			[ flow s1; flow s2;
			(try
				let inits2 = init s2
				in
				BatPSet.map
					(fun x ->
						(x, inits2,
						(match s1 with
						| StmtWithBlock (While _) -> "no"
						| _ -> "")))
					(final s1)
			with | NoProgramPoint -> BatPSet.empty) ]
	| StmtWithBlock (Cond (_, s1, s2, l)) ->
		List.fold_left BatPSet.union BatPSet.empty
			[ flow s1; flow s2;
			(try singleton (l, (init s1), "yes")
			with | NoProgramPoint -> BatPSet.empty);
			(try singleton (l, (init s2), "no")
			with | NoProgramPoint -> BatPSet.empty) ]
	| StmtWithBlock (While (_, s, l)) ->
		List.fold_left BatPSet.union BatPSet.empty
			[ flow s;
			(try singleton (l, (init s), "yes")
			with | NoProgramPoint -> BatPSet.empty);
			(try BatPSet.map (fun x -> (x, l, "")) (final s)
			with | NoProgramPoint -> singleton (l, l, "yes")) ]
	| _ -> BatPSet.empty

let flow_r flowset = BatPSet.map (fun (a, b, label) -> (b, a, label)) flowset

let flowmap flowset =
	let srcset = BatPSet.map (fun (x, _, _) -> x) flowset in
	let destset =
		BatPSet.map
			(fun i ->
				BatPSet.map (fun (_, y, _) -> y)
					(BatPSet.filter (fun (x, y, z) -> x = i) flowset))
			srcset in
	BatEnum.fold2 BatPMap.add BatPMap.empty (BatPSet.enum srcset)
		(BatPSet.enum destset)

let number stmt =
	let rec _number stmt prevlab =
		match stmt with
		| StmtWithBlock block ->
			(match block with
			| Assign (var, aexpr, _) ->
				StmtWithBlock (Assign (var, aexpr, prevlab + 1))
			| Skip _ -> StmtWithBlock (Skip (prevlab + 1))
			| Cond (bexpr, s1, s2, _) ->
				let new_s1 =
					if BatPSet.is_empty (labels s1)
					then s1
					else _number s1 (prevlab + 1) in
				let new_s2 =
					if BatPSet.is_empty (labels s2)
					then s2
					else
					if BatPSet.is_empty (labels s1)
					then _number s2 (prevlab + 1)
					else _number s2 (BatPSet.max_elt (labels new_s1))
				in StmtWithBlock (Cond (bexpr, new_s1, new_s2, prevlab + 1))
			| While (bexpr, s, _) ->
				let new_s =
					if BatPSet.is_empty (labels s)
					then s
					else _number s (prevlab + 1)
				in StmtWithBlock (While (bexpr, new_s, prevlab + 1)))
		| Seq (s1, s2) ->
			let (prevlab, new_s1) =
				if BatPSet.is_empty (labels s1)
				then (prevlab, s1)
				else
				(let _new_s1 = _number s1 prevlab
				in ((BatPSet.max_elt (labels _new_s1)), _new_s1))
			in Seq (new_s1, _number s2 prevlab)
		| Print aexpr -> Print aexpr
	in _number stmt 0

let blockmap stmt =
	let rec _labeltoblock stmt prevmap =
		match stmt with
		| StmtWithBlock block ->
			(match block with
			| Assign (_, _, label) | Skip label ->
				BatPMap.add label block prevmap
			| Cond (_, s1, s2, label) ->
				let prevmap = BatPMap.add label block prevmap
				in
				List.fold_left BatPMap.union prevmap
					[ _labeltoblock s1 BatPMap.empty;
					_labeltoblock s2 BatPMap.empty ]
			| While (_, s, label) ->
				let prevmap = BatPMap.add label block prevmap
				in
				List.fold_left BatPMap.union prevmap
					[ _labeltoblock s BatPMap.empty ])
		| Seq (s1, s2) ->
			List.fold_left BatPMap.union prevmap
				[ _labeltoblock s1 BatPMap.empty; _labeltoblock s2 BatPMap.empty ]
		| _ -> BatPMap.empty
	in _labeltoblock stmt BatPMap.empty

let blockmap_r blockmap =
	BatPMap.of_enum
		(BatEnum.map (fun (x, y) -> (y, x)) (BatPMap.enum blockmap))

let subaexp_of_aexp aexp =
	let rec _subaexp_of_aexp aexp set =
		match aexp with
		| Arith (_, e1, e2) ->
			List.fold_left BatPSet.union (BatPSet.add aexp set)
				[ _subaexp_of_aexp e1 BatPSet.empty;
				_subaexp_of_aexp e2 BatPSet.empty ]
		| _ -> set
	in _subaexp_of_aexp aexp BatPSet.empty

let subaexp_of_bexp bexp =
	let rec _subaexp_of_bexp bexp set =
		match bexp with
		| BoolOp (_, e1, e2) ->
			List.fold_left BatPSet.union set
				[ _subaexp_of_bexp e1 BatPSet.empty;
				_subaexp_of_bexp e2 BatPSet.empty ]
		| Relation (_, e1, e2) ->
			List.fold_left BatPSet.union set
				[ subaexp_of_aexp e1; subaexp_of_aexp e2 ]
		| _ -> set
	in _subaexp_of_bexp bexp BatPSet.empty

let subaexp_of_stmt stmt =
	let rec _program_aexp stmt set =
		match stmt with
		| StmtWithBlock (Assign (_, aexp, _)) ->
			BatPSet.union set (subaexp_of_aexp aexp)
		| StmtWithBlock (Skip l) -> set
		| StmtWithBlock (Cond (_, s1, s2, _)) ->
			List.fold_left BatPSet.union set
				[ _program_aexp s1 BatPSet.empty; _program_aexp s1 BatPSet.empty ]
		| StmtWithBlock (While (_, s, _)) ->
			BatPSet.union set (_program_aexp s BatPSet.empty)
		| Seq (s1, s2) ->
			List.fold_left BatPSet.union set
				[ _program_aexp s1 BatPSet.empty; _program_aexp s2 BatPSet.empty ]
		| _ -> BatPSet.empty
	in _program_aexp stmt BatPSet.empty

let rec freevariable_of_aexp aexp =
	let rec _freevariable_of_aexp aexp set =
		match aexp with
		| Var id -> BatPSet.add id set
		| Arith (_, e1, e2) ->
			List.fold_left BatPSet.union set
				[ freevariable_of_aexp e1; freevariable_of_aexp e2 ]
		| _ -> set
	in _freevariable_of_aexp aexp BatPSet.empty

let rec freevariable_of_bexp bexp =
	let rec _freevariable_of_bexp bexp set =
		match bexp with
		| BoolOp (_, e1, e2) ->
			List.fold_left BatPSet.union set
				[ freevariable_of_bexp e1; freevariable_of_bexp e2 ]
		| Relation (_, e1, e2) ->
			List.fold_left BatPSet.union set
				[ freevariable_of_aexp e1; freevariable_of_aexp e2 ]
		| _ -> set
	in _freevariable_of_bexp bexp BatPSet.empty

let rec freevariable_of_stmt stmt =
	let rec _freevariable_of_stmt stmt set =
		match stmt with
		| StmtWithBlock (Assign (_, aexp, l)) ->
			BatPSet.union set (freevariable_of_aexp aexp)
		| StmtWithBlock (Skip l) -> set
		| StmtWithBlock (Cond (_, s1, s2, _)) ->
			List.fold_left BatPSet.union set
				[ _freevariable_of_stmt s1 BatPSet.empty;
				_freevariable_of_stmt s2 BatPSet.empty ]
		| StmtWithBlock (While (_, s, _)) ->
			BatPSet.union set (freevariable_of_stmt s)
		| Seq (s1, s2) ->
			List.fold_left BatPSet.union set
				[ freevariable_of_stmt s1; freevariable_of_stmt s2]
		| _ -> set
	in _freevariable_of_stmt stmt BatPSet.empty