{
  open Parser
  exception LexerError of string
  
  let keyword_table = Hashtbl.create 53
  let _ =
    List.iter (fun (kwd, tok) -> Hashtbl.add keyword_table kwd tok)
              [ "skip", SKIP;
                "if", IF;
                "else", ELSE;
                "while", WHILE;
                "print", PRINT;
                "true", BOOL(true);
                "false", BOOL(false);]
}
let blank = [' ' '\t']+
let newline = ['\n' '\r']+
let digit = ['0'-'9']
let lower = ['a'-'z']
let upper = ['A'-'Z']
let alpha = lower | upper

let id = lower (alpha)*
let int = digit+
 
rule lexer = parse
  | blank   { lexer lexbuf }
  | newline { lexer lexbuf }
  | int     { let i = int_of_string (Lexing.lexeme lexbuf) in
              INT(i) }
  | id      { let id = Lexing.lexeme lexbuf in
              try Hashtbl.find keyword_table id with
                _ -> VAR id
            }
  | "+"     { PLUS }
  | "-"     { MINUS }
  | "*"     { TIMES }
  | "/"     { DIVIDE }
	| "%"     { MODULO }
  | "&&"    { AND }
  | "||"    { OR }
  | "<"     { LESSTHAN }
  | ">"     { GREATERTHAN }
  | "<="    { LTEQUAL }
  | ">="    { GTEQUAL }
  | "="     { EQUAL }
  | "!="    { NOTEQUAL }
  | "("     { LPAREN }
  | ")"     { RPAREN }
  | "{"     { LBRACE }
  | "}"     { RBRACE }
  | ":="    { ASSIGN }
  | ";"     { SEMICOLON }
  | eof     { EOF }
  | _       { let token = Lexing.lexeme lexbuf in
              raise (LexerError ("unrecognized token: '" ^ token ^ "'")) }
              
{}