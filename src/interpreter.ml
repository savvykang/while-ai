open Ast
open SetUtil

let bind (id : id) (set : int BatPSet.t) state = BatPMap.add id set state
let valueof (id : id) state =
	try BatPMap.find id state with _ -> singleton 0

let setofmap a = BatPSet.of_enum (BatPMap.enum a)
let mapofset a = BatPMap.of_enum (BatPSet.enum a)
let domain a = BatPSet.map (fun (x, _) -> x) (setofmap a)

let string_of_strset set =
	let out = BatIO.output_string () in
	(BatPSet.iter (fun x -> BatIO.nwrite out (Printf.sprintf "%s; " x)) set);
	BatIO.close_out out

let join (a : ('a, 'b) BatPMap.t) (b : ('a, 'b) BatPMap.t) =
	let common_keys = intersection (domain a) (domain b) in
	let conflicts x y = BatPSet.mem x common_keys in
	let (_, nojoin_a) = BatPMap.partition conflicts a in
	let (_, nojoin_b) = BatPMap.partition conflicts b in
	let add x joinset =
		BatPSet.add (x, (BatPSet.union (BatPMap.find x a) (BatPMap.find x b)))
			joinset
	in
    mapofset (BatList.reduce BatPSet.union
			[BatPSet.fold add common_keys BatPSet.empty;
			setofmap nojoin_a; setofmap nojoin_b ])

let testcase () =
	()

let rec eval_stmt trace state stmt =
	if trace
	then
	(Printer.print_stmt stmt; Printer.print_label stmt; print_string "\n");
	match stmt with
	| StmtWithBlock (Assign (id, aexpr, _)) ->
		let values = eval_aexpr state aexpr in bind id values state
	| StmtWithBlock (Skip _) -> state
	| Seq (stmt1, stmt2) ->
		eval_stmt trace (eval_stmt trace state stmt1) stmt2
	| StmtWithBlock (Cond (_, stmt1, stmt2, _)) ->
		let s1 = eval_stmt trace state stmt1 in
		let s2 = eval_stmt trace state stmt2 in join s1 s2
	| (StmtWithBlock (While (bexpr, body, _)) as self) ->
		let conditions = eval_bexpr state bexpr in
		if BatPSet.exists (fun x -> x = true) conditions
		then begin
			let changed = eval_stmt trace state body
			in
			(eval_stmt trace changed self)
		end
		else state
	| Print aexpr -> (Printer.print_set (eval_aexpr state aexpr); state)

and eval_aexpr state expr =
	match expr with
	| Int i -> singleton i
	| Var id -> valueof id state
	| Arith (op, aexpr1, aexpr2) ->
		let (v1, v2) = ((eval_aexpr state aexpr1), (eval_aexpr state aexpr2))
		and op = match op with
			| Plus -> ( + )
			| Minus -> ( - )
			| Times -> ( * )
			| Divide -> ( / )
			| Modulo -> ( mod )
		in BatPSet.map (fun (x, y) -> op x y) (product v1 v2)

and eval_bexpr state expr =
	match expr with
	| Bool b -> singleton b
	| BoolOp (op, bexpr1, bexpr2) ->
		let (v1, v2) = ((eval_bexpr state bexpr1), (eval_bexpr state bexpr2))
		and op = (match op with | And -> ( && ) | Or -> ( || ))
		in BatPSet.map (fun (x, y) -> op x y) (product v1 v2)
	| Relation (op, aexpr1, aexpr2) ->
		let (v1, v2) = ((eval_aexpr state aexpr1), (eval_aexpr state aexpr2))
		and op = match op with
			| LessThan -> ( < )
			| GreaterThan -> ( > )
			| LTEqual -> ( <= )
			| GTEqual -> ( >= )
			| Equal -> ( = )
			| NotEqual -> ( <> )
		in BatPSet.map (fun (x, y) -> op x y) (product v1 v2)
