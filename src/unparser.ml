open Ast
open BatIO
open Printf

let writethru out foo = (nwrite out foo; out)

let string_of_aexpr aexpr =
	let rec _string_of_aexpr out aexpr =
		match aexpr with
		| Int i -> writethru out (string_of_int i)
		| Var id -> writethru out id
		| Arith (op, expr1, expr2) ->
			let out1 = _string_of_aexpr out expr1 in
			let out2 =
				writethru out1
					(sprintf "%s"
						(match op with
						| Plus -> "+"
						| Minus -> "-"
						| Times -> "*"
						| Divide -> "/"
						| Modulo -> "%"))
			in _string_of_aexpr out2 expr2
	in close_out (_string_of_aexpr (output_string ()) aexpr)

let string_of_bexpr bexpr =
	let rec _string_of_bexpr out bexpr =
		match bexpr with
		| Bool b -> writethru out (string_of_bool b)
		| BoolOp (op, expr1, expr2) ->
			let out1 = _string_of_bexpr out expr1 in
			let out2 =
				writethru out1
					(sprintf "%s"
						(match op with
						| And -> "&&"
						| Or -> "||")
					)
			in _string_of_bexpr out2 expr2
		| Relation (op, expr1, expr2) ->
			List.fold_left writethru out
				[ string_of_aexpr expr1;
				sprintf "%s"
					(match op with
					| LessThan -> "<"
					| GreaterThan -> ">"
					| LTEqual -> "<="
					| GTEqual -> ">="
					| Equal -> "="
					| NotEqual -> "!=");
				string_of_aexpr expr2 ]
	in close_out (_string_of_bexpr (output_string ()) bexpr)

let string_of_set set =
	let out = output_string () in
	(BatPSet.iter (fun x -> nwrite out (Printf.sprintf "%d; " x)) set);
	close_out out

let string_of_state state =
	let out = output_string () in
	BatPMap.iter
		(fun id set ->
			nwrite out (Printf.sprintf "%s:" id);
			nwrite out (string_of_set set))
		state;
	close_out out

let string_of_block block = match block with
	| Assign (id, aexpr, _) -> sprintf "%s:=%s" id (string_of_aexpr aexpr)
	| Skip (_) -> "skip"
	| Cond (bexpr, _, _, _) -> (string_of_bexpr bexpr)
	| While (bexpr, _, _) -> (string_of_bexpr bexpr)

let string_of_stmt stmt = match stmt with
	| StmtWithBlock (block) -> string_of_block block
	| Seq (_) -> ""
	| Print aexpr -> sprintf "print %s" (string_of_aexpr aexpr)

let string_of_label stmt = match stmt with
	| StmtWithBlock (b) -> sprintf "[%d]" 0
	| _ -> ""