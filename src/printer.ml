open Unparser
open BatIO

let print stream string =
	nwrite stream string; nwrite stream "\n"; flush stream

let (_, exe) =
	let exe = BatStd.exe in
	try BatString.rsplit exe "/" with _ -> ("", exe)

let print_error string = print stderr (Printf.sprintf "%s: %s" exe string)

let print_stmt stmt = print stdout (string_of_stmt stmt)
let print_label stmt = print stdout (string_of_label stmt)
let print_aexpr aexpr = print stdout (string_of_aexpr aexpr)
let print_bexpr bexpr = print stdout (string_of_bexpr bexpr)
let print_state state = print stdout (string_of_state state)
let print_set set = print stdout (string_of_set set)