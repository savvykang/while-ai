type id = string
type label = int
type flow = (label * label * string)

type aop = | Plus | Minus | Times | Divide | Modulo
type bop = | And | Or
type rop = | LessThan | GreaterThan | LTEqual | GTEqual | Equal | NotEqual

type stmt =
  | StmtWithBlock of block
  | Seq of stmt * stmt
  | Print of aexpr

and block =
  | Assign of id * aexpr * label 
  | Skip of label
  | Cond of bexpr * stmt * stmt * label
  | While of bexpr * stmt * label

and aexpr =
  | Int of int
  | Var of id
  | Arith of aop * aexpr * aexpr

and bexpr =
  | Bool of bool
  | BoolOp of bop * bexpr * bexpr
  | Relation of rop * aexpr * aexpr

exception NoProgramPoint